import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurDivision implements EventHandler<ActionEvent>{
    private AppliSomme appli;
    
    public ControleurDivision(AppliSomme appli){
        this.appli = appli;
    }
    
    public void handle(ActionEvent e){
        System.out.println("Division");
        this.appli.diviser();
    }
}