import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.application.Platform;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class AppliSomme extends Application {

    private Label resultat;
    private TextField tf1;
    private TextField tf2;

    @Override
    public void init() {
        // cette méthode est utilisée pour initialiser les éléments 
        // non graphiques et éventuellement graphiques autres que la Scène et le Stage
        this.tf1 = new TextField();
        this.tf2 = new TextField();
        this.resultat = new Label();
    }

    private GridPane gridPane() {
        GridPane pane = new GridPane();
        pane.add(new Label("Nombre 1"), 0, 0);
        pane.add(new Label("Nombre 2"), 2, 0);
        pane.add(new Label("Résultat : "), 0, 1);
        pane.add(tf1, 1, 0);
        pane.add(tf2, 3, 0);
        pane.add(resultat, 1, 1, 3, 1);
        pane.setHgap(10);
        pane.setVgap(20);
        return pane;
    }

    private HBox hbox() {
        HBox pane = new HBox(50);
        Button boutonA = new Button("Additionner");
        Button boutonM = new Button("Multiplier");
        Button boutonD = new Button("Diviser");
        Button boutonR = new Button("Reset");
        Button boutonQ = new Button("Quitter");
        pane.getChildren().addAll(boutonA, boutonM, boutonD, boutonR, boutonQ);
        pane.setAlignment(Pos.CENTER);
        boutonA.setOnAction(new ControleurAdditionner(this));
        boutonM.setOnAction(new ControleurMultiplier(this));
        boutonD.setOnAction(new ControleurDivision(this));
        boutonQ.setOnAction(new ControleurQuitter(this));
        boutonR.setOnAction(new ControleurReset(this));
        return pane;
    }

    private HBox root() {
        HBox pane = new HBox(10);
        VBox vbox = new VBox(50);
        vbox.getChildren().addAll(gridPane(), hbox());
        vbox.setPrefWidth(700);
        HBox.setMargin(vbox, new Insets(50));
        pane.getChildren().add(vbox);
        return pane;
    }

    @Override
    public void start(Stage stage) {
        Scene scene = new Scene(root());
        stage.setTitle("Calculatrice");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Cette méthode efface les deux textfields et le label résultat
     * en y mettant une chaîne de caractère vide
     */
    public void efface() {
        this.tf1.setText("");
        this.tf2.setText("");
        this.resultat.setText("");
    }

    public void quitte() {
        Platform.exit();
    }

    /**
     * Cette méthode met à jour le label résultat avec la somme des
     * deux nombres qu'on peut récupérer dans les deux textfields
     */
    public void additionne() {

        try {
            double val1 = Double.valueOf(this.tf1.getText());
            double val2 = Double.valueOf(this.tf2.getText());
            this.resultat.setText("" + (val1 + val2));
        } catch (NumberFormatException e) {
            this.resultat.setText("Il y a une erreur");
        }

    }

    public void multiplier() {

        try {
            double val1 = Double.valueOf(this.tf1.getText());
            double val2 = Double.valueOf(this.tf2.getText());
            this.resultat.setText("" + (val1 * val2));
        } catch (NumberFormatException e) {
            this.resultat.setText("Il y a une erreur");
        }

    }

    public void diviser() {

        try {
            double val1 = Double.valueOf(this.tf1.getText());
            double val2 = Double.valueOf(this.tf2.getText());
            this.resultat.setText("" + (val1 / val2));
        } catch (NumberFormatException e) {
            this.resultat.setText("Il y a une erreur");
        }

    }
}










